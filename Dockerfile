FROM eclipse-temurin:8-jdk

ARG VERSION=1.0.0 \
    SUBVERSION=-rc2

ENV DEBIAN_FRONTEND=noninteractive

RUN groupadd -r hortonworks && \
    useradd --no-log-init -r -g hortonworks hortonworks

RUN apt-get -qq update && apt-get -qq -y --no-install-recommends install \
    gettext

RUN wget -q -O /opt/schemaregistry.tar.gz https://github.com/hortonworks/registry/releases/download/$VERSION$SUBVERSION/schemaregistry-$VERSION.tar.gz && \
    tar -C /opt -xzf /opt/schemaregistry.tar.gz && \
    rm /opt/schemaregistry.tar.gz && \
    mv /opt/schemaregistry-$VERSION /opt/schemaregistry && \
    chown -R hortonworks:hortonworks /opt/schemaregistry

WORKDIR /opt/schemaregistry

COPY entrypoint.sh .
RUN chmod a+x entrypoint.sh

RUN mkdir template
COPY registry.yaml template/registry.yaml

ENV DB_NAME schema_registry
ENV DB_USER registry_user
ENV DB_PASSWORD registry_password
ENV DB_HOST localhost

EXPOSE 9090 9091

USER hortonworks

ENTRYPOINT ["/opt/schemaregistry/entrypoint.sh"]

CMD ["/opt/schemaregistry/bin/registry-server-start.sh", "/opt/schemaregistry/conf/registry.yaml"]
