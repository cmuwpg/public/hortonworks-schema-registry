#!/bin/sh
set -e
envsubst < /opt/schemaregistry/template/registry.yaml > /opt/schemaregistry/conf/registry.yaml
# /opt/schemaregistry/bootstrap/bootstrap-storage.sh migrate
exec "$@"
